#include <OneWire.h> // Параллельное подключение датчиков
#include <DallasTemperature.h> // Датчики температуры
#include <NewPing.h> // Датчик расстояния

#define BAUD_RATE 115200 // Скорость передачи данных
#define DELAY_TIME 200 // Время паузы между передачами

// Термодатчики
#define TEMPERATURE_PIN 2
#define TEMPERATURE_PRECISION 11

// Фоторезисторы
#define LIGHT_RED_PIN A0
#define LIGHT_BLUE_PIN A1
#define LIGHT_MAX 1000
#define LIGHT_MIN 1
/*
// Датчик расстояния
#define TRIG_PIN 4
#define ECHO_PIN 5
#define MAX_DISTANCE 200 // Максимальное корректное расстояние
*/

OneWire oneWire(TEMPERATURE_PIN);
DallasTemperature sensors(&oneWire);
DeviceAddress redThermometer = { 0x28, 0xFF, 0x90, 0x84, 0x93, 0x16, 0x5, 0x9D },
              blueThermometer = { 0x28, 0xF2, 0x54, 0x1, 0x0, 0x0, 0x80, 0x8B };
/*
NewPing sonar(TRIG_PIN, ECHO_PIN, MAX_DISTANCE);
*/
void setup(void)
{
  Serial.begin(BAUD_RATE);
  sensors.begin();
  /*
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  */
  Serial.print("Device Count = ");
  Serial.print(sensors.getDeviceCount(), DEC);
  Serial.print(", ");

  Serial.print("Parasite power: ");
  if (sensors.isParasitePowerMode())
    Serial.println("ON");
  else Serial.println("OFF");

  Serial.print("Red Device Address: ");
  printAddress(redThermometer);
  Serial.println();

  Serial.print("Blue Device Address: ");
  printAddress(blueThermometer);
  Serial.println();
  Serial.println();

  sensors.setResolution(redThermometer, TEMPERATURE_PRECISION);
  sensors.setResolution(blueThermometer, TEMPERATURE_PRECISION);
}

// Печать адреса устройства
void printAddress(DeviceAddress deviceAddress)
{
  Serial.print("A=");
  for (uint8_t i = 0; i < 8; i++)
  {
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}

void loop(void)
{ 
  // Определение температуры
  sensors.requestTemperatures();
  // Передача данных термодатчиков
  Serial.println("A=28FF90849316059D D="+String(sensors.getTempC(redThermometer)));
  delay(DELAY_TIME);
  Serial.println("A=28F254010000808B D="+String(sensors.getTempC(blueThermometer)));
  delay(DELAY_TIME);

  // Определение освещённости
  int light_red_raw = analogRead(LIGHT_RED_PIN);
  int light_blue_raw = analogRead(LIGHT_BLUE_PIN);
  int light_red = map(light_red_raw, 0, 1024, LIGHT_MIN, LIGHT_MAX);
  int light_blue = map(light_blue_raw, 0, 1024, LIGHT_MIN, LIGHT_MAX);
  
  // Передача данных фоторезисторов
  Serial.println("A=LIGHT0RED00PIN01 D="+String(light_red)); 
  delay(DELAY_TIME);
  Serial.println("A=LIGHT0BLUE0PIN02 D="+String(light_blue));
  delay(DELAY_TIME);
/*
  // Определение расстояния
  unsigned int distance = sonar.ping_cm();
  Serial.println("A=SONAR0DISTANCE01 D="+String(distance));
*/
  // Пауза
  delay(DELAY_TIME);
}
