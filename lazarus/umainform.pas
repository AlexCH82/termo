unit umainform;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, TAGraph, TAChartListbox, TASeries,
  TARadialSeries, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls, Buttons, ActnList, SdpoSerial, LazSerial;

type

  { TMainForm }

  TMainForm = class(TForm)
    ActConnect: TAction;
    ActGetData: TAction;
    ActInit: TAction;
    ActClear: TAction;
    ActTabChange: TAction;
    ActShowData: TAction;
    ActList: TActionList;
    ChartIllumination: TChart;
    SeriesL1: TLineSeries;
    SeriesL2: TLineSeries;
    ClearBtn: TButton;
    ChartTemperature: TChart;
    GBControl: TGroupBox;
    PageControl: TPageControl;
    Panel: TPanel;
    PortComboBox: TComboBox;
    SdpoSerial: TSdpoSerial;
    SeriesT1: TLineSeries;
    SeriesT2: TLineSeries;
    ChartListbox: TChartListbox;
    GBData: TGroupBox;
    LblData1: TLabel;
    LblData2: TLabel;
    TabSheetDistance: TTabSheet;
    TabSheetIllumination: TTabSheet;
    TabSheetTemperature: TTabSheet;
    Timer: TTimer;
    ToggleBox: TToggleBox;
    procedure ActClearExecute(Sender: TObject);
    procedure ActInitExecute(Sender: TObject);
    procedure ActTabChangeExecute(Sender: TObject);
    procedure ActShowDataExecute(Sender: TObject);
    procedure ActConnectExecute(Sender: TObject);
    procedure ActGetDataExecute(Sender: TObject);
  private
    { private declarations }
    SensorData: AnsiString;
    t: LongInt;
    t1, t2: Extended;
    l1, l2: Extended;
  public
    { public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.ActConnectExecute(Sender: TObject);
begin
  try
    SdpoSerial.Device:=PortComboBox.Text;
    SdpoSerial.Active:=ToggleBox.Checked;
  except
    ToggleBox.Checked:=False;
  end;
  Timer.Enabled:=ToggleBox.Checked;
  ActTabChange.Execute;
  PortComboBox.Enabled:=not ToggleBox.Checked;
  if ToggleBox.Checked then
    begin
      ToggleBox.Caption:='ВЫКЛ';
      ToggleBox.Color:=clRed;
    end
  else
    begin
      ToggleBox.Caption:='ВКЛ';
      ToggleBox.Color:=clGreen;
    end;
end;

procedure TMainForm.ActShowDataExecute(Sender: TObject);
begin
  if ((t1>-300.0) or (t2>-300.0)) or ((l1>-300.0) or (l2>-300.0)) then Inc(t);
  if t1>-300 then
    SeriesT1.AddXY(t,t1);
  if t2>-300 then
    SeriesT2.AddXY(t,t2);
  if l1>-300 then
    SeriesL1.AddXY(t,l1);
  if l2>-300 then
    SeriesL2.AddXY(t,l2);
  case PageControl.PageIndex of
    0: begin
         if t1>-300.0 then LblData1.Caption:=Format('%-5.1f °C',[t1])
         else LblData1.Caption:='Н/Д';
         if t2>-300.0 then LblData2.Caption:=Format('%-5.1f °C',[t2])
         else LblData2.Caption:='Н/Д';
         LblData1.Enabled:=SeriesT1.Active;
         LblData2.Enabled:=SeriesT2.Active;
       end;
    1: begin
         if l1>-300.0 then LblData1.Caption:=Format('%-5.1f лк',[l1])
         else LblData1.Caption:='Н/Д';
         if l2>-300.0 then LblData2.Caption:=Format('%-5.1f лк',[l2])
         else LblData2.Caption:='Н/Д';
         LblData1.Enabled:=SeriesL1.Active;
         LblData2.Enabled:=SeriesL2.Active;
       end;
    2: begin
         ;
       end;
  end;
end;

procedure TMainForm.ActInitExecute(Sender: TObject);
begin
  t:=-1;
  t1:=-300.0;
  t2:=-300.0;
  l1:=-300.0;
  l2:=-300.0;
end;

procedure TMainForm.ActTabChangeExecute(Sender: TObject);
begin
  case PageControl.PageIndex of
    0: ChartListbox.Chart:=ChartTemperature;
    1: ChartListbox.Chart:=ChartIllumination;
    2: ;
  end;
end;

procedure TMainForm.ActClearExecute(Sender: TObject);
begin
  SeriesT1.Clear;
  SeriesT2.Clear;
  SeriesL1.Clear;
  SeriesL2.Clear;
  ActInit.Execute;
end;

procedure TMainForm.ActGetDataExecute(Sender: TObject);
const
  ID_TEMPERATURE_RED  = '28FF90849316059D';
  ID_TEMPERATURE_BLUE = '28F254010000808B';
  ID_LIGHT_RED  = 'LIGHT0RED00PIN01';
  ID_LIGHT_BLUE = 'LIGHT0BLUE0PIN02';
var
  id: String; //AnsiString;
  p: Integer;
  data: Extended;
begin
  SensorData:='';
  while SdpoSerial.DataAvailable do
    SensorData:=SensorData+SdpoSerial.ReadData;
  {$IFDEF WINDOWS}
  SensorData[Pos('.', SensorData)]:=',';
  {$ENDIF}
  p:=0;
  data:=-300.0;
  id:='';
  p:=Pos('A=', SensorData);
  if p<>0 then id:=Copy(SensorData, p+2, 16);
  p:=Pos('D=', SensorData);
  if p<>0 then data:=StrToFloatDef(Copy(SensorData, p+2, 5), -300.0);
  if id=ID_TEMPERATURE_RED then t1:=data;
  if id=ID_TEMPERATURE_BLUE then t2:=data;
  if id=ID_LIGHT_RED then l1:=data;
  if id=ID_LIGHT_BLUE then l2:=data;
end;


end.

